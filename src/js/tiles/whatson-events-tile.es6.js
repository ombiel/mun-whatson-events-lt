const { LiveTile, registerTile, getLibraries } = require("@ombiel/exlib-livetile-tools");
const screenLink = require('@ombiel/aek-lib/screen-link');
const AekStorage = require("@ombiel/aek-lib/storage");

const momentTimezone = require("moment-timezone");
const moment = require("moment");

let storage = false;
let authStorageKey = false;
let storageKey = false;
let cacheStorageKey = false;

// pull in custom CSS
const eventsCSS = require("../../css/events");

// define variables in the root scope of your module
let _;

// when fulfilled, assign them to the variables defined above
// we also assign the returned promise to a variable we can reference later
let libsReady = getLibraries(['lodash', 'jquery']).then((libs) => {
  [_] = libs;
});

moment.locale('en', {
  calendar: {
    lastDay: '[Yesterday]',
    sameDay: '[Today]',
    nextDay: '[Tomorrow]',
    lastWeek: '[Last] dddd',
    nextWeek: 'dddd',
    sameElse: 'L'
  }
});

class WhatsonEventsTile extends LiveTile {

  onReady() {
    this.throttledRender = _.throttle(this.render, 1000);
    this.showOriginalFace(() => {
      this.setOptions();
      this.init();
    });
  }

  init() {
    let fetchData = true;

    let now = moment();
    if (this.dataCacheDataExpiry) {
      // let fallback = 'Australia/Sydney';
      let timezone = momentTimezone.tz.guess();
      let timeNow = momentTimezone.tz(now, timezone);

      // use the cached expiry to build a timestamp
      let cachedTimestamp = moment(this.dataCacheDataExpiry, "X");
      cachedTimestamp = momentTimezone.tz(cachedTimestamp, timezone);

      // check if we've reached the expiry date and invalidate fetchData if necessary
      if (cachedTimestamp.isAfter(timeNow)) {
        fetchData = false;
      }
    }

    let delay = this.timer(this.initPause, () => { }).promise;
    Promise.all([libsReady, delay]).then(() => {
      if (fetchData) {
        // integrate any delays set by the tile attributes
        this.fetchData();
      } else {
        // lastUpdated is only set in cache on a successful service call, so we can guarantee its existing
        let data = storage.get(storageKey);
        this.cachedUpdate(data);
      }
    });
  }

  translateLocationCode(code) {
    switch (code) {
      case "MURDOCH":
        this.location = "perth";
        break;
      case "ROCKINGHAM":
        this.location = "rockingham";
        break;
      case "MANDURAH":
        this.location = "mandurah";
        break;
      case "DUBAI-ISC":
        this.location = "dubai";
        break;
      case "KAPLAN-SGP":
        this.location = "singapore";
        break;
      case "KAPLAN-MMR":
        this.location = "myanmar";
        break;
      default:
        this.location = false;
    }
  }

  fetchData() {
    if(!this.dataLoading) {
      this.dataLoading = true;
      // we don't want a throttledRender; we always want to make sure the loading spinner is displayed
      this.render();

      this.fetchCourses();
    }
  }

  fetchCourses() {
    let url = screenLink('mun-whatson-events/courses');
    this.ajax({ url: url })
    .done((res) => {
      console.warn("courses response :: ");
      console.log(res);

      if(res && _.has(res, "response.courseList") && res.response.courseList.length > 0) {
        this.translateLocationCode(res.response.courseList[0].locationCode);
        this.courseLevel = res.response.courseList[0].courseLevel;
      }
      
      this.fetchStudent();
    });
  }

  fetchStudent() {
    let url = screenLink('mun-whatson-events/student');
    this.ajax({ url: url })
    .done((res) => {
      console.warn("student response :: ");
      console.log(res);

      if (res && _.has(res, "response.data")) {
        this.studentType = res.response.data.studentType;
      }

      this.fetchEvents();
    });
  }

  fetchEvents() {
    let url = screenLink('mun-whatson-events/data');
    this.ajax({ url: url, data: { location: this.location, courseLevel: this.courseLevel, studentType: this.studentType } })
    .done((res) => {
      console.warn("events response :: ");
      console.log(res);

      if (res && _.has(res, "response.eventsList")) {
        if (res.response.eventsList.length > 0) {
          this.update(res);
        } else {
          // Empty Data
          this.showOriginalFace();
          this.dataLoading = false;
        }
      } else {
        this.errorRender();
      }
    }).always(() => {
      if (this.fetchTimer) { this.fetchTimer.stop(); }
      // refreshTimer is a common way to control live tile data flow, but Murdoch want to define it based on cache expiry
      // make sure we handle refreshTimer explicitly being disabled, or if we have a cache expiry set instead
      if (this.dataCacheDays === 0 && this.dataCacheHours === 0 && this.refreshTimer > 0) {
        this.fetchTimer = this.timer(this.refreshTimer, this.fetchData.bind(this));
      }
    });
  }

  getCMAttrs() {
    if (storage && authStorageKey) {
      return storage.get(authStorageKey);
    } else {
      return false;
    }
  }

  getCache() {
    if (storage && storageKey) {
      return storage.get(storageKey);
    } else {
      return false;
    }
  }

  update(res) {
    let data = res.response.eventsList;

    this.dataLoading = false;
    if (data && !_.isEqual(data, this.data)) {
      this.data = data;
      this.throttledRender();

      if (storage && storageKey) {
        storage.set(storageKey, data);
      }

      if (this.dataCacheDays > 0 || this.dataCacheHours > 0) {
        let now = moment();
        let timezone = momentTimezone.tz.guess();
        let timeNow = momentTimezone.tz(now, timezone);

        if (this.trackHoursAndDays) {
          timeNow.add(this.dataCacheDays, "days");
          timeNow.add(this.dataCacheHours, "hours");
        } else if (this.dataCacheDays > 0) {
          // prioritise days setting over hours, if set 
          timeNow.add(this.dataCacheDays, "days");
        } else if (this.dataCacheHours > 0) {
          timeNow.add(this.dataCacheHours, "hours");
        }

        // console.warn("setting expiry to " + timeNow.format());
        if (storage && cacheStorageKey) {
          storage.set(cacheStorageKey, timeNow.format("X"));
        }
      } else {
        // we can assume both options have deliberately been set to 0 or otherwise disabled; remove the limiter
        if (cacheStorageKey) {
          for (let key in window.localStorage) {
            if (key.indexOf(cacheStorageKey) > -1) {
              window.localStorage.removeItem(key);
              break;
            }
          }
        }
      }
    }

    if (storage && authStorageKey && res.attrs) {
      storage.set(authStorageKey, res.attrs);
    }
  }

  cachedUpdate(data) {
    this.dataLoading = false;
    if (data && !_.isEqual(data, this.data)) {
      this.data = data;
      // we want to force an immediate render from the cache
      this.render();
    }
  }

  errorRender() {
    this.dataLoading = false;

    this.error = this.errorText;
    // we want to force an immediate render in the event of an error
    this.render();
  }

  render() {
    let content;

    let tileAttributes = this.getTileAttributes();
    let tileWidth = tileAttributes.tileWidth ? tileAttributes.tileWidth : 2;
    if (this.dataLoading) {
      let textClasses = eventsCSS.loading;
      if (tileWidth == 2) {
        textClasses += " " + eventsCSS.reduced;
      }

      content = `
          <div class="${textClasses}">
            <p>${this.loadingText}</p>
          </div>
        `;

    } else if (this.error) {
      // default to the smaller width just to ensure that nomatter what, all content is visible
      let textClasses = eventsCSS.error;
      if (tileWidth == 2) {
        textClasses += " " + eventsCSS.reduced;
      }

      content = `
        <div class = "${eventsCSS.faceContainer}" >
          <div class="${textClasses}"> 
            <p>${this.errorText}</p>
          </div>
        </div>
        `;

    } else {
      let data = this.data.find((event) => event.rank === 1);
      // console.log(data);

      if (data === undefined) {
        let textClasses = eventsCSS.error;
        if (tileWidth == 2) {
          textClasses += " " + eventsCSS.reduced;
        }

        content = `
          <div class = "${eventsCSS.faceContainer}">
            <div class="${textClasses}"> 
              <p>${this.emptyText}</p>
              </div>
            </div>
          </div>
          `;
      } else {
        let imageURL = data.thumbnailUrl ? data.thumbnailUrl : "";

        let locationCode = data.location && data.location.code ? data.location.code : "N/A";
        let encodedDataURL = data.url ? "campusm://openURL?url=" + encodeURIComponent(data.url) : false;
        let aekToolbarUrl = false;
        if(this.aekMenuReference && this.aekMenuReference.length > 0) {
          aekToolbarUrl = "campusm://loadaek?toolbar=" + this.aekMenuReference;
        }

        content = `
            <div class="${eventsCSS.faceContainer}">
              <div class="${eventsCSS.whatsOnEvents}">
                <a href="#" data-hotspot=${encodedDataURL} class="${eventsCSS.imageHotspot}">
                  <div class="${eventsCSS.image}" style="background-image: url(${imageURL})"/>
                </a>
                <div class="${eventsCSS.rightPane}">
                  <a href="#" data-hotspot=${encodedDataURL} class="${eventsCSS.textHotspot}">
                    <div class="${eventsCSS.article}">
                      <p class="${eventsCSS.latestEvent}">${this.titleText}</p>
                      <p class="${eventsCSS.title}">${data.title}</p>
                      <p class="${eventsCSS.date}">${moment(data.startDateTime).format("DD MMM, YYYY")}</p>
                      <p class="${eventsCSS.date}">${moment(data.startDateTime).format("H.mm")} - ${moment(data.endDateTime).format("H.mm")} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LOC: ${locationCode}</p>
                    </div>
                  </a>
                  <a href="#" data-hotspot="${aekToolbarUrl}" class="${eventsCSS.button}">
                    <h2 class="${eventsCSS.buttonText}">${this.buttonText}</h2>
                  </a>
                </div>
              </div>
            </div>`;
      }
    }

    this.currentFace = this.flipFace(content);
    this.currentFace.onHotspot((url) => {
      // console.log(url);
      if(url) {
        this.openURL(url);
      }
    });
  }

  validatePossibleZeroOption(name, option) {
    let success = false;

    // standard || fallback seems to be triggering for an initPause of 0; we want to be able to set this to 0
    if (typeof option === "number" && option > -1) {
      this[name] = option;
      success = true;
    } else if (option === false) {
      // remember to handle explicitly setting it to false, any exceptions can be set after the fact
      this[name] = 0;
      success = true;
    }

    return success;
  }

  setOptions() {
    let tileAttributes = this.getTileAttributes();
    let whatsonOptions = tileAttributes.whatson || {};

    let storageBaseString = whatsonOptions.storageBaseString || "murdoch_portal";
    storage = new AekStorage(storageBaseString); // prefixes "_aek_store_" to any key, so "_aek_store_murdoch_mydetails"

    // AEKStorage adds "__" between the name of the storage - storageBaseString - and the key value we're actually setting in localStorage
    authStorageKey = whatsonOptions.authStorageKey || "cmAttrs"; // "_aek_store_murdoch_mydetails__cmAttrs"
    storageKey = whatsonOptions.storageKey || "events"; // "_aek_store_murdoch_mydetails__profile"

    this.aekMenuReference = whatsonOptions.aekMenuReference && whatsonOptions.aekMenuReference.length > 0 ? whatsonOptions.aekMenuReference : "";

    this.animationPause = parseInt(whatsonOptions.pause) || 3000;
    this.initPause = this.animationPause;
    // standard || fallback seems to be triggering for an initPause of 0; we *want* to be able to set this to 0
    if (typeof whatsonOptions.initPause === "number" && whatsonOptions.initPause > -1) {
      this.initPause = parseInt(whatsonOptions.initPause);
    }

    // validate any unsupported value for this, and default the cache to a single day if necessary
    let dataCacheDaysValidated = this.validatePossibleZeroOption("dataCacheDays", whatsonOptions.dataCacheDays);
    if (!dataCacheDaysValidated) {
      this.dataCacheDays = 1;
    }

    let dataCacheHoursValidated = this.validatePossibleZeroOption("dataCacheHours", whatsonOptions.dataCacheHours);
    if (!dataCacheHoursValidated) {
      this.dataCacheHours = 0;
    }

    // force this to be a boolean
    this.trackHoursAndDays = whatsonOptions.trackHoursAndDays === true ? true : false;

    cacheStorageKey = whatsonOptions.cacheStorageKey || "dataExpiry";
    cacheStorageKey = storageKey + "_" + cacheStorageKey;
    this.dataCacheDataExpiry = storage.get(cacheStorageKey);
    if (!this.dataCacheDataExpiry) {
      this.dataCacheDataExpiry = false;
    }

    this.loadingText = whatsonOptions.loadingText || "We’re fetching the latest events...";
    this.errorText = whatsonOptions.errorText || "Oops! We have having technical issues. Please try again later.";
    this.emptyText = whatsonOptions.emptyText || "There are no events to display right now. Please check back later.";
    this.titleText = whatsonOptions.titleText || "EVENTS";
    this.buttonText = whatsonOptions.buttonText || "VIEW ALL EVENTS";
    this.refreshTimer = parseInt(whatsonOptions.refreshTimer) || 600000;

    // explicitly declaring values expected in render() to avoid complications with undefined
    this.dataLoading = false;
    this.data = false;
    this.attrs = false;
  }

}

registerTile(WhatsonEventsTile, "whatsOnEvents");
